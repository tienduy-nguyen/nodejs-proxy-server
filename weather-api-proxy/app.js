const express = require('express');
const morgan = require('morgan');
const middlewares = require('./middlewares');
const api = require('./api');

require('dotenv').config();

const app = express();
app.set('trust proxy', 1);
app.use(morgan('dev'));
app.use(express.json());

app.get('/', (req, res) => {
  res.json({
    message: '🦄🌈✨👋🌎🌍🌏✨🌈🦄',
  });
});

app.use('/api/v1', api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
