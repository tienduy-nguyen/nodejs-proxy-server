const express = require('express')
const proxy = require('http-proxy-middleware')

var app = express()
const options = {
  target: 'http://target.com/', // target host
  changeOrigin: true, // needed for virtual hosted sites
  router: {
    'localhost:3000': 'http://target.com/'
  }
}

// small hack
app.options('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')
  res.send(200)
})

app.use(proxy(options))

app.listen(3030)
