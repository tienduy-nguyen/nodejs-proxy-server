const express = require('express');
const fetch = require('node-fetch');

const PORT = process.env.PORT || 4545;

const app = express();

const cachedData = {
  username: '',
  data: {},
  expiredTime: Date.now(),
};

// Set response
function setResponse(username, repos) {
  return `<h2>${username} has ${repos} Github repos</h2>`;
}

// Make request to Github for data
async function getRepos(req, res, next) {
  try {
    console.log('Fetching data...');
    const { username } = req.params;
    const response = await fetch(`https://api.github.com/users/${username}`);
    const data = await response.json();
    const repos = data.public_repos;

    // SEt memory data
    cachedData.username = req.params;
    cachedData.expireTime = Date.now() + 30 * 1000; // 30 s
    cachedData.data = repos;

    res.send(setResponse(username, repos));
  } catch (error) {
    console.error(error);
    res.status(500);
    res.send({
      errors: {
        message: error.message,
        stack: error.stack,
      },
    });
  }
}

// Cache middleware
function cache(req, res, next) {
  const { username } = req.params;
  if (
    cachedData?.username === username &&
    cachedData?.expiredTime > Date.now()
  ) {
    return res.json(cachedData.data);
  }
  next();
}

app.get('/', (req, res) => {
  res.send('Hi there!');
});

app.get('/repos/:username', cache, getRepos);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
