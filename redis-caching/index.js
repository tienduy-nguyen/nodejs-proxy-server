const express = require('express');
const fetch = require('node-fetch');
const Redis = require('ioredis');
const { response } = require('express');

const PORT = process.env.PORT || 4545;
const REDIS_PORT = process.env.PORT || 6379;

const redisClient = new Redis();

const app = express();

// Set response
function setResponse(username, repos) {
  return `<h2>${username} has ${repos} Github repos</h2>`;
}

// Make request to Github for data
async function getRepos(req, res, next) {
  try {
    console.log('Fetching data...');
    const { username } = req.params;
    const response = await fetch(`https://api.github.com/users/${username}`);
    const data = await response.json();
    const repos = data.public_repos;

    // SEt data to redis to catching
    redisClient.set(username, repos, 'EX', 3600); // 1h --> 'EX' for second, 'PX' for millisecond
    res.send(setResponse(username, repos));
  } catch (error) {
    console.error(error);
    res.status(500);
    res.send({
      errors: {
        message: error.message,
        stack: error.stack,
      },
    });
  }
}

// Cache middleware
function cache(req, res, next) {
  const { username } = req.params;
  redisClient.get(username, (err, data) => {
    if (err) throw err;

    if (data !== null) {
      res.send(setResponse(username, data));
    } else {
      next();
    }
  });
}

app.get('/', (req, res) => {
  res.send('Hi there!');
});

app.get('/repos/:username', cache, getRepos);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
